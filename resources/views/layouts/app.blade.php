<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>AJAX CRUD</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="icon" href="favicon/light_favicon.svg">
</head>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script>
    //favicon change event
    window
        .matchMedia("(prefers-color-scheme: dark)")
        .addEventListener("change", (event) => {
            if (event.matches) {
                const favicon = document.querySelector("link[rel~='icon']");
                favicon.href = "favicon/light_favicon.svg";
            } else {
                const favicon = document.querySelector("link[rel~='icon']");
                favicon.href = "favicon/favicon.png";
            }
        });
    const theme = window.matchMedia("(prefers-color-scheme: dark)").matches
        ? "dark"
        : "light";
    const favicon = document.querySelector("link[rel~='icon']");
    if (theme === "dark") {
        favicon.href = "favicon/light_favicon.svg";
    } else {
        favicon.href = "favicon/favicon.png";
    }

</script>
<body style="margin-top: 60px" class="container">

@yield('content')

<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha512-vtqEYD3CAJMe31p7FahQtJdmd3OJyZ4gGK5rMmVS8x0FrGrUSq+VZieIVnlNcjwNxcQAn54Q8aR/CUCX0WnQfQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
{{--<script src="js/4.0.0-alpha.5.bootstrap.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>--}}
<script src="js/laracrud.js"></script>
</body>
</html>
