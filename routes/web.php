<?php

use App\Models\Link;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//--LOAD THE VIEW--//
Route::get('/', function () {
    $links = Link::all();
    return view('laracrud')->with('links', $links);
});

//--CREATE a link--//
Route::post('/links', function () {
    $link = Link::create(request()->all());
    return Response::json($link);
});

//--GET LINK TO EDIT--//
Route::get('/links/{link_id?}', function ($link_id) {
    $link = Link::find($link_id);
    return Response::json($link);
});

//--UPDATE a link--//
Route::put('/links/{link_id?}', function ($link_id) {
    $link = Link::findOrFail($link_id);
    $link->update(request()->all());
    return Response::json($link);
});

//--DELETE a link--//
Route::delete('/links/{link_id?}', function ($link_id) {
    $link = Link::destroy($link_id);
    return Response::json($link);
});
